const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch()
  const page = await browser.newPage()
  await page.goto('https://www.yahoo.com/')
  console.log(await page.evaluate(() =>
      [...document.querySelectorAll('meta')]
        .map(elem => {
          const attrObj = {};
          elem.getAttributeNames().forEach(name => {
            attrObj[name] = elem.getAttribute(name);
          })
          return attrObj;
        })
    ));
  await browser.close()
})()
