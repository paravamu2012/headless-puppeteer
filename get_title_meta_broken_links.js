const puppeteer = require('puppeteer');
var blc = require('broken-link-checker');

let customData = {};
let options = {};
let pageUrl = 'https://www.yahoo.com'

var htmlUrlChecker = new blc.HtmlUrlChecker(options, {
	html: function(tree, robots, response, pageUrl, customData){},
	junk: function(result, customData){},
	link: function(result, customData){
    if (result.broken) {
	       console.log('Broken Link: ' + result.url['resolved']); //=> HTTP_404
     } else if (result.excluded) {
	       console.log(result.excludedReason); //=> BLC_ROBOTS
    }
  },
	page: function(error, pageUrl, customData){},
	end: function(){}
});

htmlUrlChecker.enqueue(pageUrl, customData);

(async () => {
  const browser = await puppeteer.launch()
  const page = await browser.newPage()
  await page.goto(pageUrl)
  const title = await page.title()
  console.log('Page Title:' + title)
  console.log(await page.evaluate(() =>
      [...document.querySelectorAll('meta')]
        .map(elem => {
          const attrObj = {};
          elem.getAttributeNames().forEach(name => {
            attrObj[name] = elem.getAttribute(name);
          })
          return attrObj;
        })
    ));
  await browser.close()
})()
