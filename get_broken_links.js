var blc = require('broken-link-checker');

let customData = {};
let options = {};
let pageUrl = 'https://www.yahoo.com'

var htmlUrlChecker = new blc.HtmlUrlChecker(options, {
	html: function(tree, robots, response, pageUrl, customData){},
	junk: function(result, customData){},
	link: function(result, customData){
    if (result.broken) {
	       console.log('Broken Link: ' + result.url['resolved']); //=> HTTP_404
     } else if (result.excluded) {
	       console.log(result.excludedReason); //=> BLC_ROBOTS
    }
  },
	page: function(error, pageUrl, customData){},
	end: function(){}
});

htmlUrlChecker.enqueue(pageUrl, customData);
